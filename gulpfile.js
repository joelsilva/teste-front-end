var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var googleWebFonts = require('gulp-google-webfonts');
var less = require('gulp-less');
var runSequence = require('run-sequence');

var files = {
    html: ['site/index.html'],
    less: ['site/assets/**.less',],
    fontes: ['site/assets/fonts.list'],
    css: ['./site/css']
};

gulp.task('clean', function () {
    return gulp.src(files.css)
        .pipe(clean());
});

gulp.task('copy-fa', function(){
    return gulp.src('./node_modules/font-awesome/**/**')
        .pipe(gulp.dest('./site/css/font-awesome'));
});

gulp.task('fonts', function () {
    return gulp.src(files.fontes)
        .pipe(googleWebFonts({}))
        .pipe(gulp.dest('./site/css/'));
});

gulp.task('less', function () {
    return gulp.src(files.less)
        .pipe(less())
        .pipe(concat('estilo.css'))
        .pipe(gulp.dest('./site/css/'));
});

gulp.task('browserReload', function(){
    browserSync.reload();
});

gulp.task('serve', ['copy-fa', 'fonts', 'less'], function(){
   browserSync.init({
       server:{
           baseDir: "./site/"
       }
   });

   gulp.watch(files.html, ['browserReload']);
   gulp.watch(files.less, ['less', 'browserReload']);
});

gulp.task('default', function(){
    return runSequence('clean', ['serve']);
});